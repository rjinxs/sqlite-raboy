var app = angular.module('app.controller', []);

app.controller("PeopleListCtrl", function($scope, $cordovaSQLite) {
 
  $scope.selectAll = function() {
    var query = "SELECT firstname, lastname FROM people";
    $cordovaSQLite.execute(db, query, []).then(function(res) {
      if(res.rows.length > 0) {
        for(var i = 0; i < res.rows.length; i++) {
          console.log("SELECTED -> " + res.rows.item(i).firstname + " " + res.rows.item(i).lastname);
        }
      } else {
        console.log("No results found");
      }
    }, function (err) {
      console.error(err);
    });
  }
 
});