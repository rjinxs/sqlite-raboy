// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic','ngCordova','app.controller']);

var db = null;

app.run(function($ionicPlatform, $cordovaSQLite) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    window.plugins.sqlDB.copy("example.db", 0, function() {
      db = $cordovaSQLite.openDB("example.db");
    }, function(error) {
      console.error("There was an error copying the database: " + error);
      db = $cordovaSQLite.openDB("example.db");
    });
  });
});

app.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
  .state('peoplelist',{
    url: '/people',
    templateUrl: 'templates/people-list.html',
    controller: 'PeopleListCtrl'
  })
  
  $urlRouterProvider.otherwise('/people');
});